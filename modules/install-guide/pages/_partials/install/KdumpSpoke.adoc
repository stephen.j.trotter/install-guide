:experimental:

[[sect-installation-gui-kdump]]
=== Kdump

[NOTE]
====

This screen is disabled by default. To enable it during the installation, you must use the [option]#inst.kdump_addon=on# option at the boot menu. See xref:advanced/Boot_Options.adoc#sect-boot-options-advanced[Advanced Installation Options] for details, and xref:install/Booting_the_Installation.adoc#sect-boot-menu[The Boot Menu] for instructions on using custom boot options.

====

Use this screen to select whether or not [application]*Kdump* will be activated on the installed system, and how much memory will be reserved for it if enabled.

.Kdump

image::anaconda/KdumpSpoke.png[The Kdump configuration screen, showing 128 MB reserved]

[application]*Kdump* is a kernel crash dumping mechanism which, in the event of a system crash, captures the contents of the system memory at the moment of failure. This captured memory can then be analyzed to find the cause of the crash. If [application]*Kdump* is enabled, it must have a small portion of the system's memory (RAM) reserved to itself. This reserved memory will not be accessible to the main kernel.

To enable [application]*Kdump* on the installed system, check `Enable kdump`. Then, enter the amount of memory to be reserved in megabytes into the `Memory To Be Reserved` field.

The amount of memory which you should reserve is determined based on your system's architecture (AMD64 and Intel{nbsp}64 will have different requirements than IBM Power, for example) as well as the total amount of system memory. In most cases, automatic reservation will be satisfactory. If you insist on manual settings, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Kernel_Crash_Dump_Guide/appe-supported-kdump-configurations-and-targets.html#sect-kdump-memory-requirements++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Kernel Crash Dump Guide] for guidelines. This document also contains more in-depth information about how [application]*Kdump* works, how to configure additional settings, and how to analyze a saved crash dump.

The `Usable System Memory` readout below the reservation input field shows how much memory will be accessible to your main system once your selected amount of RAM is reserved.

[NOTE]
====

Additional settings, such as the location where kernel crash dumps will be saved, can only be configured after the installation using either the `system-config-kdump` graphical interface, or manually in the `/etc/kdump.conf` configuration file.

====

After configuring [application]*Kdump* settings, click btn:[Done] in the top left corner to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-summary[Installation Summary].
