
:experimental:

[[sect-installation-gui-installation-destination-add-iscsi]]
==== Add iSCSI Target

To use iSCSI storage devices, the installer must be able to discover them as _iSCSI targets_ and be able to create an iSCSI session to access them. Both of these steps may require a user name and password for _Challenge Handshake Authentication Protocol_ (CHAP) authentication.

You can also configure an iSCSI target to authenticate the iSCSI initiator on the system to which the target is attached (_reverse CHAP_), both for discovery and for the session. Used together, CHAP and reverse CHAP are called _mutual CHAP_ or _two-way CHAP_. Mutual CHAP provides the greatest level of security for iSCSI connections, particularly if the user name and password are different for CHAP authentication and reverse CHAP authentication.

Follow the procedure below to add an iSCSI storage target to your system.

[[proc-installation-gui-installation-destination-add-iscsi]]
.Add iSCSI Target
. Click the `Add iSCSI Target` button in the bottom right corner of the xref:Installing_Using_Anaconda.adoc#sect-installation-gui-installation-destination[Installation Destination - Specialized & Network Disks] screen. A new dialog window titled `Add iSCSI Storage Target` will open.

. Enter the IP address of the iSCSI target in the `Target IP Address` field.

. Provide a name in the `iSCSI Initiator Name` field for the iSCSI initiator in _iSCSI Qualified Name_ (IQN) format. A valid IQN entry contains:
+
** The string `iqn.` (including the period).
+
** A date code specifying the year and month in which your organization's Internet domain or subdomain name was registered, represented as four digits for the year, a dash, and two digits for the month, followed by a period. For example, represent September 2010 as `2010-09.`
+
** Your organization's Internet domain or subdomain name, presented in *reverse* order (with the top-level domain first). For example, represent the subdomain storage.example.com as `com.example.storage`.
+
** A colon (`:`) followed by a string which uniquely identifies this particular iSCSI initiator within your domain or subdomain. For example, `:diskarrays-sn-a8675309`
+
A complete IQN will therefore look as follows:
+
[subs="quotes, macros"]
----
`iqn.2010-09.com.example.storage:diskarrays-sn-a8675309`
----
+
An example using the correct format is also displayed below the input field for reference.
+
For more information about IQNs, see [citetitle]_3.2.6. iSCSI Names in RFC 3720 - Internet Small Computer Systems Interface (iSCSI)_, available from link:++https://tools.ietf.org/html/rfc3720#section-3.2.6++[] and [citetitle]_1. iSCSI Names and Addresses in RFC 3721 - Internet Small Computer Systems Interface (iSCSI) Naming and Discovery_, available from link:++https://tools.ietf.org/html/rfc3721#section-1++[].

. Specify the type of authentication to use for iSCSI discovery using the `Discovery Authentication Type` drop-down menu. Depending on which type of authentication you selected, additional input fields (such as `CHAP Username` and `CHAP Password` may then become visible. Fill in your authentication credentials; these should be provided by your organization.

. Click the `Start Discovery` button. The installer will now attempt to discover an iSCSI target based on the information you provided, and if the target requires CHAP or reverse CHAP authentication, it will attempt to use the credentials you provided. This process may take some time (generally less than 30 seconds), depending on your network.
+
If the discovery was *not* successful, an error message will be displayed in the dialog window. This message will vary based on which part of the discovery failed. If the installer did not find the target you specified at all, you should check the IP address; if the problem is an authentication error, make sure you entered all CHAP and reverse CHAP credentials correctly and that you have access to the iSCSI target.
+
[NOTE]
====

The `No nodes discovered` error message may also mean that all nodes on the address you specified are already configured. During discovery, [application]*Anaconda* ignores nodes which have already been added.

====
+
If the discovery was successful, you will see a list of all discovered nodes.

. Select one or more nodes you want to log in to by marking or unmarking the check box next to each node discovered on the target. Below the list, select again the type of authentication you want to use; you can also select the `Use the credentials from discovery` option if the CHAP/reverse CHAP user name and password you used to discover the target are also valid for logging in to it.
+
After selecting all nodes you want to use, click `Log In` to initiate an iSCSI session. [application]*Anaconda* will attempt to log in to all selected nodes. If the login process is successful, the `Add iSCSI Storage Target` dialog will close, and all nodes you have configured will now be shown in the list of network disks in xref:Installing_Using_Anaconda.adoc#sect-installation-gui-installation-destination[Installation Destination - Specialized & Network Disks].

You can repeat this procedure to discover additional iSCSI targets, or to add more nodes from a previously configured target. However, note that once you click the `Start Discovery` button for the first time, you will not be able to change the `iSCSI Initiator Name`. If you made an error when configuring the initiator name, you must restart the installation.
